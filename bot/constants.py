import re

# estas variables son usadas para los stages de un conversación
# son globales, así que es importante que no colisionen
class CONVERSATIONS:
    # este primero lo usamos para cuando no queremos ningún handler salvo los fallback
    FALLBACK, \
    ADMIN_ADMIT, \
    ADMIN_REJECT = range(3)
    LAST = ADMIN_REJECT

    # crear constates dinámicamente así se definen donde se usan y no hay que
    # tocar nada acá
    def create_consts(n):
        ret_range = [i for i in range(CONVERSATIONS.LAST+1, CONVERSATIONS.LAST+1+n)]
        CONVERSATIONS.LAST += n
        return ret_range if len(ret_range) > 1 else ret_range[0]

USERNAME_REGEX = re.compile('^[a-zA-Z0-9]+(?:_[a-zA-Z0-9]+)*$')
