from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (
    CommandHandler,
    CallbackQueryHandler,
    ConversationHandler,
    MessageHandler,
    Filters,
)
from bot.constants import CONVERSATIONS
from bot.db import Vars
from bot.utils import escape_markdown_entities
from bot.data_store import MensajesPendientes
from .cancel import cancel

# nombre de comando (sin /)
COMMAND = 'start'

# constantes de states
POST, CONFIRM = CONVERSATIONS.create_consts(2)

# constantes de botones (namespace casero con "<comando>_<n>")
YES, EDIT, CANCEL = [f'{COMMAND}_{i}' for i in range(3)]

def create_handler():
    return ConversationHandler(
        entry_points=[CommandHandler(COMMAND, entry_command)],
        states={
            POST: [MessageHandler((Filters.text & ~Filters.command), post_handler)],
            CONFIRM: [
                CallbackQueryHandler(post_yes, pattern='^' + YES + '$'),
                CallbackQueryHandler(post_edit, pattern='^' + EDIT + '$'),
                CallbackQueryHandler(post_cancel, pattern='^' + CANCEL + '$'),
            ],
        },
        fallbacks=[
            CommandHandler('cancel', cancel),
        ],
    )

def entry_command(update, context):
    if not Vars.get_admin_group():
        reply_text = 'Hola! Este bot todavía no fue configurado, paciencia...\n\n' \
            'Si este mensaje persiste contactate con les admins de RLab'
        update.message.reply_text(reply_text)
        return ConversationHandler.END

    if MensajesPendientes.has_mine(update, context):
        reply_text = 'Hola! Veo que ya tenés un mensaje esperando a ser validado\n\n' \
            'Te voy a mandar un mensaje cuando se apruebe, saludos!'
        update.message.reply_text(reply_text)
        return ConversationHandler.END

    reply_text = 'Hola yo soy el bot posteador de @CotDRLab 📬\n\n' \
        'Mandame el texto que quieras, formateado si querés, y yo lo envío al canal\n\n' \
        'Al finalizar te voy a mostrar cómo queda y te voy a pedir confirmación\n\n' \
        'Ahora mandame el texto (o cancelá con /cancel):'
    update.message.reply_text(reply_text)
    return POST

def post_handler(update, context):
    text = update.message.text_markdown_v2_urled
    context.chat_data['texto_mensaje'] = text
    update.message.reply_text(f'Tu mensaje se va a ver así:\n{text}', parse_mode='MarkdownV2')

    # InlineKeyboardButton -  https://python-telegram-bot.readthedocs.io/en/stable/telegram.inlinekeyboardbutton.html
    keyboard = [
        [
            InlineKeyboardButton("Sí", callback_data=YES),
            InlineKeyboardButton("Modificar", callback_data=EDIT),
        ],
        [InlineKeyboardButton("Cancelar", callback_data=CANCEL)],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    reply_text = '⏺ ¿Querés que lo envíe?'
    update.message.reply_text(reply_text, reply_markup=reply_markup)

    return CONFIRM

def post_yes(update, context):
    texto_mensaje = context.chat_data['texto_mensaje']
    username = update.effective_user.username

    MensajesPendientes.add_mine(update, context, texto_mensaje)

    # NOTA: este mismo código se usa en admin_reject.py función render_main_menu !
    msg = f'Lx usuarix @{escape_markdown_entities(username)} quiere postear el siguiente mensaje:\n{texto_mensaje}'

    # InlineKeyboardButton -  https://python-telegram-bot.readthedocs.io/en/stable/telegram.inlinekeyboardbutton.html
    keyboard = [
        [
            InlineKeyboardButton("Admitir", callback_data=f'{CONVERSATIONS.ADMIN_ADMIT}-{username}'),
            InlineKeyboardButton("Rechazar", callback_data=f'{CONVERSATIONS.ADMIN_REJECT}-{username}'),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    context.bot.send_message(
        chat_id=Vars.get_admin_group(),
        text=msg,
        parse_mode='MarkdownV2',
        reply_markup=reply_markup)

    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    query.answer()
    reply_text = '✅ Mensaje enviado a les moderadores, te voy a avisar cuando sea aceptado\n\n' \
        'Si en algún momento querés volver a mandar algo hacé /start de vuelta, saludos!'
    query.edit_message_text(reply_text)

    return ConversationHandler.END

def post_edit(update, context):
    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    query.answer()

    reply_text = '⏺ Perfecto. Volveme a mandar el texto corregido:'
    query.edit_message_text(reply_text)

    return POST

def post_cancel(update, context):
    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    query.answer()

    reply_text = '✴️ Envío cancelado\n\n' \
        'Cualquier cosa si se te ocurre volver a postear algo hacé /start de vuelta, saludos!'
    query.edit_message_text(reply_text)

    return ConversationHandler.END
