from telegram.ext import CallbackQueryHandler, ConversationHandler

from bot.constants import CONVERSATIONS
from bot.db import Vars
from bot.data_store import MensajesPendientes

def create_handler():
    return CallbackQueryHandler(command, pattern='^' + str(CONVERSATIONS.ADMIN_ADMIT) + '-.*')

def command(update, context):
    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    callback_data = query.data
    username = callback_data.split('-')[1]

    # traemos data de la bot_data
    mensaje_pendiente = MensajesPendientes.get_user(context, username)

    # mandamos al canal
    context.bot.send_message(
        chat_id=Vars.get_post_channel(),
        text=mensaje_pendiente.msg,
        parse_mode='MarkdownV2')

    # le avisamos al usuarie
    context.bot.send_message(
        chat_id=mensaje_pendiente.chat_id,
        text='✅ Tu mensaje fue aceptado y enviado al canal @CotDRLab')

    query.answer()
    query.edit_message_text('✅ Mensaje envíado con éxito')

    # borramos de la bot_data
    MensajesPendientes.remove_user(context, username)

    return ConversationHandler.END
