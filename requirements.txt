# TG BOT - https://github.com/python-telegram-bot/python-telegram-bot
python-telegram-bot

# DB ORM - https://github.com/coleifer/peewee
peewee

# Load .env - https://pypi.org/project/python-dotenv/
python-dotenv

# File change watcher - https://github.com/gorakhargosh/watchdog
watchdog
